#SOURCE is a list of source files
#FLAGS is a list of flags to use in the compiler
#EXEC is the name of the executable
#COMPILER is the compiler to use

SOURCE := $(shell find ./*.cpp)
FLAGS = -Wall -o $(EXEC)
EXEC = driver
COMPILER = g++

run: driver input.txt
	./driver < input.txt

driver: $(SOURCE)
	g++ $(SOURCE) $(FLAGS)
