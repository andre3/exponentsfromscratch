/*
  Header file for log functions
*/
#ifndef LOGS_H
#define LOGS_H

//Desired number of bits of precision
#define PRECISION 50

//Calculates the log base 2 of a double
double logTwo(double);

//Calculates log of a variable base
double logBase(double base, double num);



#endif
