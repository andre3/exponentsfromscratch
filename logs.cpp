/*
  Source file for logarithmic functions, no dependencies
*/

#include "logs.h"

double logTwo(double input) {
  int i;
  int count;
  //divBy represents the binary place value of the digit being calculated
  unsigned long long int divBy = 1;
  //addNum is where each term in the infinite series is stored before it is
  //added to the final anwer
  double addNum;
  //The final answer, initialized to zero because it is the sum of a series
  double ans = 0.0;
  /*
    This method of calcuating logarithms only works when the number is greater
    than 1, so this loop multiplies the input by two enough times to make
    it greater than 1, and keeps track of this by subtracting from the final
    answer. Validity shown in this:
      log (base 2) (a/2) = log (base 2) (a) - log (base 2) (2)
      = log (base 2) (a) - 1
    Therefore, every time we multiply the input by 2, we subtract one from the
    final answer.
  */
  while (input < 1) {
    input *= 2;
    ans--;
  }
  /*
    We calculate the logarithm using the following method:
      Count the number of time (count) s we can divide input by 2
      Divide this by the number representing the place value
      Add this to the final answer
      Take the new input and square it (raise it to the 2)
      Repeat until you reach desired preision (for a double, 50 binary digits)
    This calculates one digit at a time.
  */
  for (i = 0; i < PRECISION; i++) {
    count = 0;
    while (input >= 2) {
      count += 1;
      input = input / 2;
    }
    addNum = static_cast<double> (count) / divBy;
    divBy = divBy<<1;
    ans += addNum;
    input *= input;
  }
  return ans;
}

//Compute logTwo for both the base and the number and then use change of base
//log base b of a = log base c of a / log base c of b
double logBase(double base, double input) {
  return (base == 2) ? logTwo(input) : logTwo(input)/logTwo(base);
}
