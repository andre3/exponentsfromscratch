/*
  Header file for exponential functions
*/
#ifndef EXPONENTS_H
#define EXPONENTS_H
//Exponential function depends on logarithmic functions
#include "logs.h"

#define E_VALUE 2.71828182846


using namespace std;


//Calculates e to the power of pow
double expon(double pow);

//Calculates base to the power of pow
//Uses a^b = e^(b(ln(a)))
double power(double base, double pow);

#endif
