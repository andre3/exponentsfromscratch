#include "driver.h"

int main() {
  double base;
  double num;
  cout << "Enter a base followed by the number" << endl;
  cin >> base >> num;
  cout << "log base " << base << " of " << num << " = " << logBase(base,num)
   << endl;
  cout << base << " ^ " << num << " = " << power(base,num) << endl;
  return 0;
}
