/*
  Source file for exponential functions
  Algorithm used depends on logarithmic functions
*/

#include "exponents.h"



double expon(double base) {
  double nFac = 1;
  int i;
  double x = 1;
  double error = 10;
  double ans = 1;
  double add = 10;
  double ePow = 1;
  //find e^floor(x) + 1; used to find an upper bound on error
  for (i = static_cast<int> (base) + 1; i > 0; --i) {
    ePow *= E_VALUE;
  }

  i = 0;
  //calculate sum of (x^n)/(n)! until error is good enough
  //Time taken depends on base
  while (error > 0.000001 * ans) {
    error = add * ePow;
    x *= base;
    i++;
    nFac *= i;
  //  cout << x << '/' << nFac << endl;
    add = x/nFac;
    ans += add;
    //error = add * ePow;
  }
  return ans;
}

double power(double base, double pow) {
  return expon(pow * logBase(E_VALUE, base));
}
